#  XamarinManiaLib.Logger

Przygotował Dawid Musialik.

Celem stworzenia tej aplikacji było napisanie biblioteki, która będzie wykorzystywana w aplikacjach mojego autorstwa. Ma ona za zadanie ułatwić pracę z pluginem NLog (https://github.com/NLog/NLog).

### Instalacja
- Android
Należy dodać odwołanie do projektu "XamarinManiaLib.LoggerSetup_Android". Następnie w głównej klasie aplikacji wywołać metodę: "SetupWithCheckPermissionsAsync". Na przykład
    - await LogerSetup.SetupWithCheckPermissionsAsync("/XamarinMania/[nazwa_twojego_projektu]/Log/", false);
- UWP
Należy dodać odwołanie do projektu “XamarinManiaLib.LoggerSetup_UWP”. Następnie w głównej klasie aplikacji wywołać metodę: “SetupWithCheckPermissionsAsync”. Na przykład
    - await LogerSetup.SetupWithCheckPermissionsAsync("/XamarinMania/[nazwa_twojego_projektu]/Log/", false);

- Xamarin.Forms
Należy dodać odwołanie do projektu “XXamarinManiaLib.Logger”. Oraz dyrektywy w klasach, które będą używały biblioteki: 
    - using XamarinManiaLib.Logger.Instance;
    - using XamarinManiaLib.Logger.Model.Enum;


### Użycie
* LoggerInstance.Instance.Log(LogType.File, LogLevel.Info, "Użycie biblioteki do obsługi NLog");

     - LogType - określa czy logi mają się zapisywać do pliku, czy wyświetlać na konsoli
     - LogLevel - poziom logu
