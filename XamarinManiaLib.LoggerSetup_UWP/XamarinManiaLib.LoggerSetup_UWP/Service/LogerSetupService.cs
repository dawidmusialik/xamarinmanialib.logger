﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinManiaLib.Logger.Instance;
using XamarinManiaLib.LoggerSetup_UWP.Interface;

namespace XamarinManiaLib.LoggerSetup_UWP.Service
{
    internal class LogerSetupService : ILogerSetupInterface
    {
        public LogerSetupService() { }

        public void _Setup(string LogFolderPath, ref string _Path, bool OnlyDebugMode)
        {
            _Path = CreateFolderAndPath(LogFolderPath);

            LoggerInstance.Instance.Setup(_Path, OnlyDebugMode);
        }

        public async Task<string> _SetupAsync(string LogFolderPath, bool OnlyDebugMode)
        {
            string _Path = "";

            _Path = CreateFolderAndPath(LogFolderPath);

            await LoggerInstance.Instance.SetupWithCheckPermissions(_Path, OnlyDebugMode);

            return _Path;
        }

        private string CreateFolderAndPath(string LogFolderPath)
        {
            string DefaultPath = Windows.Storage.ApplicationData.Current.LocalFolder.Path;

            // string folderPath = "/XamarinMania/DogHealthBook/";

            System.IO.Directory.CreateDirectory(DefaultPath + LogFolderPath);

            string LogsPath = DefaultPath + LogFolderPath;

            StringBuilder LogsPathBuilder = new StringBuilder(LogsPath);
            LogsPathBuilder.Replace("//", "/");

            return LogsPathBuilder.ToString();
        }
    }
}
