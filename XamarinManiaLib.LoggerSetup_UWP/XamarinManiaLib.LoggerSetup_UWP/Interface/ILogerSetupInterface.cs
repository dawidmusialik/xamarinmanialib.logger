﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinManiaLib.LoggerSetup_UWP.Interface
{
    internal interface ILogerSetupInterface
    {
        void _Setup(string LogFolderPath, ref string _Path, bool OnlyDebugMode);
        Task<string> _SetupAsync(string LogFolderPath, bool OnlyDebugMode);
    }
}
