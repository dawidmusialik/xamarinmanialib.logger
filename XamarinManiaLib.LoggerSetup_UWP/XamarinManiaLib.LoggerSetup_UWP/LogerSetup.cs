﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinManiaLib.Logger.Instance;
using XamarinManiaLib.LoggerSetup_UWP.Service;

namespace XamarinManiaLib.LoggerSetup_UWP
{
    public class LogerSetup
    {
        private string _Path;
        private LogerSetupService LogerSetupService;

        public LogerSetup()
        {
            _Path = "";
            LogerSetupService = new LogerSetupService();
        }

        public void Setup(bool OnlyDebugMode)
        {
            LogerSetupService._Setup("/Log/", ref _Path, OnlyDebugMode);
        }

        public void Setup(string LogFolderPath, bool OnlyDebugMode)
        {
            LogerSetupService._Setup(LogFolderPath, ref _Path, OnlyDebugMode);
        }

        public async Task SetupWithCheckPermissionsAsync(bool OnlyDebugMode)
        {
            _Path = await LogerSetupService._SetupAsync("/Log/", OnlyDebugMode);
        }

        public async Task SetupWithCheckPermissionsAsync(string LogFolderPath, bool OnlyDebugMode)
        {
            _Path = await LogerSetupService._SetupAsync(LogFolderPath, OnlyDebugMode);
        }


        public string GetPath()
        {
            StringBuilder _String = new StringBuilder(_Path);
            _String.Replace("//", "/");
            _String.Replace("\\", "/");
            _String.Replace("\"", "/");

            return _String.ToString();
        }
    }
}
