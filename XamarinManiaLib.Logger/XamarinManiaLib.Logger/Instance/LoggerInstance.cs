﻿using XamarinManiaLib.Logger.Service;
using XamarinManiaLib.Logger.Model;
using XamarinManiaLib.Logger.Model.Enum;
using System.Threading.Tasks;

namespace XamarinManiaLib.Logger.Instance
{
    public sealed class LoggerInstance
    {
        private static LoggerInstance _Instance = null;
        private LoggerSetupService _Setup = null;
        private LoggerModel _Logger = null;
        private bool _LogerActived = false;
        private bool _LoggerSetupCompleted = false;

        public static LoggerInstance Instance
        { 
            get
            {
                if (_Instance == null)
                {
                    _Instance = new LoggerInstance();
                }
                return _Instance;
            }
        }

        private LoggerInstance()
        {
            _Logger = new LoggerModel();
        }

        public void Setup(string logsPath, bool onlyDebugMode)
        {
            if(!_LoggerSetupCompleted)
            {
                LoggerSetup(logsPath, onlyDebugMode);
                _LoggerSetupCompleted = true;
            }
        }

        public async Task SetupWithCheckPermissions(string logsPath, bool onlyDebugMode)
        {
            bool PermissionsStatus = false;

            CheckPermissionsStorageService CheckPermistions = new CheckPermissionsStorageService();
            PermissionsStatus = await CheckPermistions.StartChecking();

            if (PermissionsStatus)
            {
                if (!_LoggerSetupCompleted)
                {
                    LoggerSetup(logsPath, onlyDebugMode);
                    _LoggerSetupCompleted = true;
                }
            }
        }

        public void Log(LogType logType, LogLevel logLevel, string logMessage)
        {
            if(_LogerActived) _Logger.LogMessage(logType, logLevel, logMessage);
        }

        private void LoggerSetup(string logsPath, bool onlyDebugMode)
        {
            _Setup = new LoggerSetupService(logsPath, onlyDebugMode);
            _LogerActived = true;
        }
    }

}
