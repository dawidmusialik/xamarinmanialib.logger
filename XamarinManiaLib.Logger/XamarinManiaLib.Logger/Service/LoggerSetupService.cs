﻿using System;

using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.Logger.Service
{
    internal class LoggerSetupService
    {
        internal LoggerSetupService(string logsPath, bool onlyDebugMode)
        {
            if (onlyDebugMode == true && string.IsNullOrWhiteSpace(logsPath) == false)
            {
                if(System.Diagnostics.Debugger.IsAttached)
                {
                    LoggerConfigurationService Configuration = new LoggerConfigurationService(logsPath);

                    NLog.LogManager.Configuration = Configuration.GetConfig();
                }
            }
            else if(onlyDebugMode == false && string.IsNullOrWhiteSpace(logsPath) == false)
            {
                LoggerConfigurationService Configuration = new LoggerConfigurationService(logsPath);

                NLog.LogManager.Configuration = Configuration.GetConfig();
            }
        }
    }
}
