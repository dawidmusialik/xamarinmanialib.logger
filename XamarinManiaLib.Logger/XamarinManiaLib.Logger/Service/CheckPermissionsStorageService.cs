﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace XamarinManiaLib.Logger.Service
{
    internal class CheckPermissionsStorageService
    {
        internal CheckPermissionsStorageService()
        {

        }

        internal async Task<bool> StartChecking()
        {
            var statusWriteExternalStorage = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
            if (statusWriteExternalStorage != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                if (results.ContainsKey(Permission.Storage))
                    statusWriteExternalStorage = results[Permission.Storage];
            }

            if (statusWriteExternalStorage == PermissionStatus.Granted) return true;
            else return false;
        }
    }
}
