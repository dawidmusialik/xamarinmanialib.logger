﻿using NLog;

namespace XamarinManiaLib.Logger.Service
{
    internal class LoggerConfigurationService
    {
        private NLog.Config.LoggingConfiguration Config;
        private NLog.Targets.FileTarget TargetFile;
        private NLog.Targets.ConsoleTarget TargetConsole;
        private string LogPath;

        internal LoggerConfigurationService(string logPath)
        {
            LogPath = logPath;
            CreateItem();
            SetItem();
        }

        internal NLog.Config.LoggingConfiguration GetConfig()
        {
            return Config;
        }

        internal void CreateItem()
        {
            Config = new NLog.Config.LoggingConfiguration();
            TargetFile = new NLog.Targets.FileTarget("logfile");
            TargetConsole = new NLog.Targets.ConsoleTarget("logconsole");
        }
        internal void SetItem()
        {
            SetItemTargetFileLog();
            SetItemTargetConsole();
        }

        internal void SetItemTargetFileLog()
        {
            TargetFile.FileName = LogPath + "/Log.txt";
            TargetFile.ArchiveFileName = LogPath + "/Log{#}.txt";
            TargetFile.Layout = "${longdate}|${level:uppercase=true}|${logger}|${message}";
            TargetFile.MaxArchiveFiles = 50;
            TargetFile.ArchiveEvery = NLog.Targets.FileArchivePeriod.Day;
            TargetFile.KeepFileOpen = false;
            TargetFile.ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.Date;
            TargetFile.ConcurrentWrites = true;
            //TargetFileLog.ArchiveAboveSize = 10;

            Config.AddRule(LogLevel.Trace, LogLevel.Fatal, TargetFile, "File");
        }

        private void SetItemTargetConsole()
        {
            Config.AddRule(LogLevel.Trace, LogLevel.Fatal, TargetConsole, "Console");
        }

    }
}