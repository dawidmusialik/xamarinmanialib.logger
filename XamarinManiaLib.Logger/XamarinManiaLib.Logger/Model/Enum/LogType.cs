﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.Logger.Model.Enum
{
    public enum LogType
    {
        File = 0,
        Console = 1
    }
}
