﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.Logger.Model.Enum
{
    public enum LogLevel
    {
        Info = 0,
        Trace = 1,
        Debug = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }
}
