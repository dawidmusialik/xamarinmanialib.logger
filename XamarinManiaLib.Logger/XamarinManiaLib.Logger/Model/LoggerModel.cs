﻿using NLog;
using XamarinManiaLib.Logger.Model.Enum;
using LogLevel = XamarinManiaLib.Logger.Model.Enum.LogLevel;

namespace XamarinManiaLib.Logger.Model
{
    internal class LoggerModel
    {
        private static NLog.Logger loggerFile = LogManager.GetLogger("File");
        private static NLog.Logger loggerConsole = LogManager.GetLogger("Console");

        internal LoggerModel() { }

        internal void LogMessage(LogType logType, LogLevel logLevel , string message)
        {
            SendMessageToLogger(logType, logLevel, message);
        }

        private void SendMessageToLogger(LogType logType, LogLevel logLevel, string message)
        {
            switch (logLevel)
            {
                case LogLevel.Info:
                    GetLogger(logType).Info(message);
                    break;
                case LogLevel.Trace:
                    GetLogger(logType).Trace(message);
                    break;
                case LogLevel.Debug:
                    GetLogger(logType).Debug(message);
                    break;
                case LogLevel.Warn:
                    GetLogger(logType).Warn(message);
                    break;
                case LogLevel.Error:
                    GetLogger(logType).Error(message);
                    break;
                case LogLevel.Fatal:
                    GetLogger(logType).Fatal(message);
                    break;
            }
        }
        private NLog.Logger GetLogger(LogType log)
        {
            switch (log)
            {
                case LogType.File:
                    return loggerFile;
                case LogType.Console:
                    return loggerConsole;
            }

            return null;
        }
    }
}