﻿using Android.Content.PM;
using System;
using System.Text;
using System.Threading.Tasks;
using XamarinManiaLib.Logger.Instance;
using XamarinManiaLib.LoggerSetup_Android.Service;

namespace XamarinManiaLib.LoggerSetup_Android
{
    public class LogerSetup
    {
        private string _Path;
        private LogerSetupService LogerSetupService;
        public LogerSetup()
        {
            _Path = "";
            LogerSetupService = new LogerSetupService();
        }

        public void Setup(bool OnlyDebugMode)
        {
            LogerSetupService._Setup("/Log/", ref _Path, OnlyDebugMode);
        }
        
        public void Setup(ApplicationInfo _ApplicationInfo, PackageManager _PackageManager, bool OnlyDebugMode)
        {
            LogerSetupService._Setup("/" + _ApplicationInfo.LoadLabel(_PackageManager) + "/", ref _Path, OnlyDebugMode);
        }


        public void Setup(string LogFolderPath, bool OnlyDebugMode)
        {
            LogerSetupService._Setup(LogFolderPath, ref _Path, OnlyDebugMode);
        }

        public async Task SetupWithCheckPermissionsAsync(bool OnlyDebugMode)
        {
            _Path = await LogerSetupService._SetupAsync("/Log/", OnlyDebugMode);
        }

        public async Task SetupWithCheckPermissionsAsync(ApplicationInfo _ApplicationInfo, PackageManager _PackageManager, bool OnlyDebugMode)
        {
            _Path = await LogerSetupService._SetupAsync("/" + _ApplicationInfo.LoadLabel(_PackageManager) + "/", OnlyDebugMode);
        }

        public async Task SetupWithCheckPermissionsAsync(string LogFolderPath, bool OnlyDebugMode)
        {
            _Path = await LogerSetupService._SetupAsync(LogFolderPath, OnlyDebugMode);
        }

        public string GetPath()
        {
            StringBuilder _String = new StringBuilder(_Path);
            _String.Replace("//", "/");
            _String.Replace("\\", "/");
            _String.Replace("\"", "/");

            return _String.ToString();
        }
    }
}
