﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace XamarinManiaLib.LoggerSetup_Android.Interface
{
    internal interface ILogerSetupInterface
    {
        void _Setup(string LogFolderPath, ref string _Path, bool OnlyDebugMode);
        Task<string> _SetupAsync(string LogFolderPath, bool OnlyDebugMode);
    }
}