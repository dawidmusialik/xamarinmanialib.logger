﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using XamarinManiaLib.Logger.Instance;
using XamarinManiaLib.LoggerSetup_Android.Interface;

namespace XamarinManiaLib.LoggerSetup_Android.Service
{
    internal class LogerSetupService : ILogerSetupInterface
    {
        public void _Setup(string LogFolderPath, ref string _Path, bool OnlyDebugMode)
        {
            _Path = CreateFolderAndPath(LogFolderPath);

            LoggerInstance.Instance.Setup(_Path, OnlyDebugMode);
        }

        public async Task<string> _SetupAsync(string LogFolderPath, bool OnlyDebugMode)
        {
            string _Path = "";

            _Path = CreateFolderAndPath(LogFolderPath);

            await LoggerInstance.Instance.SetupWithCheckPermissions(_Path, OnlyDebugMode);

            return _Path;
        }


        private static string CreateFolderAndPath(string LogFolderPath)
        {
            var LogsPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + LogFolderPath;

            StringBuilder LogsPathBuilder = new StringBuilder(LogsPath);
            LogsPathBuilder.Replace("//", "/");

            System.IO.Directory.CreateDirectory(LogsPathBuilder.ToString());

            return LogsPathBuilder.ToString();
        }
    }
}